import React, { useEffect, useState } from "react";
import "./CatScreen.css";
import ReactPaginate from "react-paginate";

const CatScreen = () => {

    const [dataCats, setDataCats] = useState([]);
    const [page, setPage] = useState(1);

    useEffect(() => {
        const updateCatsValue = async () => {
            setDataCats(await fetchCats(page));
        }
        updateCatsValue();
    }, [page]);

    const fetchCats = async (currentPage) => {
        const res = await fetch(
          // eslint-disable-next-line no-template-curly-in-string
          `https://api.thecatapi.com/v1/images/search?page=${currentPage}&order=Desc&limit=8`,{
          method: "GET",
          headers: {
              "x-api-key": "6cbe6b4a-cf75-4b84-8650-6887cf4f5fe7"
          },
          }
        )
        const data = await res.json();
        return data;
      };

      const handlePageChange = async (data) => {
        let currentPage = data.selected + 1;
        const setCurrentPage = await fetchCats(currentPage);
        setDataCats(setCurrentPage);
      };

    return (
        <>
        <h1>Liste de chats</h1>
        <div className="catcontain">
        {dataCats.map(cat => (
            
            // eslint-disable-next-line jsx-a11y/alt-text
            <img key={cat.id} src={cat.url}/>
            
        ))}
        </div>
        <ReactPaginate
            previousLabel={'Précédent'}
            nextLabel={'Suivant'}
            containerClassName={'button_contain'}
            onPageChange={handlePageChange}
            pageCount={20}
            pageClassName={'page'}
            disabledClassNae={'disabled'}
            activeClassName={'active'}
        />
        </>
    )
}

export default CatScreen;